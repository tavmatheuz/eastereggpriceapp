import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    Container,
    Header,
    Title,
    Content,
    Button,
    Item,
    Label,
    Input,
    Body,
    Left,
    Right,
    Icon,
    Form,
    Text
  } from "native-base";
  import {  StyleSheet, TextInput, NavigationActions, Image, Dimensions } from "react-native";
  
  
class EggDescriptionView extends Component {
    constructor(props) {
        super(props);

    }

    render() {
        const {params} = this.props.navigation.state;
        return (
            <Container style={styles.container}>
                <Header>
                    <Left>
                        <Button
                            transparent
                            onPress={() => this.props.navigation.goBack()}
                        >
                        <Icon name="arrow-back" />
                        </Button>
                    </Left>
                    <Body>
                        <Title>{params.item.name}</Title>
                    </Body>
                    <Right />
                </Header>

                <Content>
                    
                    <Image
                        style={styles.image}
                        source={{uri: params.item.image}}
                    />
                    
                    <Text style={styles.text}>{"R$ " + params.item.price}</Text>
        
                </Content>
          </Container>
        );
    }
}

const deviceHeight = Dimensions.get("window").height;
    
const styles = StyleSheet.create({
    container: {
        flex: 1,
    backgroundColor: "#FFF"
  },
  text: {
    alignSelf: "center",
    marginBottom: 7,
    textAlign:"center",
    marginTop:16
  },
  marginBottom: {
    marginBottom: 15
  },
  image:{
    resizeMode: "contain",
    width: null,
    height: deviceHeight/2.5,
    flex: 1
  }
})

export default EggDescriptionView;