import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    Container,
    Header,
    Title,
    Content,
    Button,
    Icon,
    ListItem,
    Text,
    Left,
    Right,
    Body,
    Card,
    CardItem
  } from "native-base";
  import {  StyleSheet, FlatList, Image } from "react-native";

  const datas = [
      {
      name:"Ovo Alpino",
      image:"https://statics-americanas.b2w.io/produtos/01/02/special/121761332/img/ovo.png",
        price:34.99
  },{
    name:"Ovo Serenata de Amor",
    image:"https://images-americanas.b2w.io/produtos/01/00/sku/32220/3/32220381_1SZ.jpg",
      price:39.99
},{
    name:"Ovo Galak",
    image:"https://images-americanas.b2w.io/spacey/2017/03/09/OVOBRANCO.jpg",
      price:39.99
},{
    name:"Ovo Frozen",
    image:"https://images-americanas.b2w.io/spacey/2017/03/16/OVODELICE.jpg",
      price:37.99
},{
    name:"Ovo Ariel",
    image:"https://1.bp.blogspot.com/-oOhrNN7oK08/WMN6Oxb1akI/AAAAAAAAG4o/eCrnxjPHrK0juusVRv5g_T8tQrzEn6KaACLcB/s1600/17191459_1128396763937438_4020650158953741598_n.jpg",
      price:49.99
},{
    name:"Ovo Chokkie",
    image:"https://geekpublicitario.com.br/wp-content/uploads/2017/04/chokkie-ovo-de-pascoa-outback-destaque-825x432.jpg",
      price:79.99
},{
    name:"Ovo Mickey",
    image:"https://i.pinimg.com/originals/64/33/f5/6433f595ac963ad7b291d74b2a5a8174.jpg",
      price:54.99
},{
    name:"Ovo de Bacon",
    image:"https://segredosdomundo.r7.com/wp-content/uploads/2017/04/destaque-20-758x455.jpg",
      price:49.99
},{
    name:"Ovo Gourmet",
    image:"https://abrilexame.files.wordpress.com/2016/09/size_960_16_9_le-vin.jpg?quality=70&strip=info&w=920",
      price:129.99
}
  ];

class EggListView extends Component {
    constructor(props) {
        super(props);

        this.state = {
            datas
          };
    }

    render() {
        const { navigate } = this.props.navigation;

        return (
    <Container style={styles.container}>
        <Header>
          <Left>
            <Button
                transparent
                onPress={() => navigate("DrawerOpen")}
                >
                <Icon name="menu" />
            </Button>
          </Left>
          <Body>
            <Title>Lista de Ovos</Title>
          </Body>
          <Right />
        </Header>
        <Content>
          <FlatList
            data={this.state.datas}
            keyExtractor={(item, index) => String(index)}
            renderItem={({ item, index }) => {
              return (
                <ListItem
                  onPress={() => navigate("EggDescriptionView",{item})}
                >
                    <Card style={styles.marginBottom}>
                        <CardItem
                            header
                            button
                            onPress={() => navigate("EggDescriptionView",{item})}
                        >
                        <Text>{item.name}</Text>
                        </CardItem>

                        <CardItem cardBody>
                            <Image
                                style={styles.image}
                                source={{uri: item.image}}
                            />
                        </CardItem>
                    
                        <CardItem
                            footer
                            button
                            onPress={() => navigate("EggDescriptionView",{item})}
                        >
                            <Icon
                                active
                                name="cart"
                                style={styles.cartIcon}
                            />
                            <Text>{"R$ " + item.price}</Text>
                        </CardItem>

                    </Card>
                </ListItem>
              );
            }}
          />
        </Content>
    </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
    backgroundColor: "#FFF"
  },
  text: {
    alignSelf: "center",
    marginBottom: 7
  },
  marginBottom: {
    marginBottom: 15
  },
  image:{
    resizeMode: "contain",
    width: null,
    height: 200,
    flex: 1
  },
  cartIcon:{ 
      color: "#777", 
      fontSize: 26, 
      width: 30 
    }
})

export default EggListView;