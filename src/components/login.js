import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    Container,
    Header,
    Title,
    Content,
    Button,
    Item,
    Label,
    Input,
    Body,
    Left,
    Right,
    Icon,
    Form,
    Text
  } from "native-base";
  import {  StyleSheet, TextInput } from "react-native";
  
class LoginView extends Component {
    constructor(props) {
        super(props);

    }

    render() {
        const { navigate } = this.props.navigation;
        return (
            <Container style={styles.container}>
                <Content padder justifyContent>
                    <Form>
                        <Item floatingLabel>
                        <Label>Username</Label>
                        <Input keyboardType="email-address" 
                        autoCorrect={false}
                        autoComplete={false}
                                autoCapitalize="none"
                                returnKeyType = {"next"}
                                onSubmitEditing={() => { this.passwordInput._root.focus(); }}
                                />
                        </Item>
                        <Item floatingLabel>
                        <Label>Password</Label>
                        <Input secureTextEntry 
                                getRef={(input) => { this.passwordInput = input; }}
                                returnKeyType = {"send"}
                                onSubmitEditing={() => { navigate("EggListView") }}/>
                        </Item>
                    </Form>
                    <Button block style={{ margin: 15, marginTop: 50 }}
                    onPress={() => 
                        navigate("EggListView")
                    }>
                        <Text>Sign In</Text>
                    </Button>
                </Content>
            </Container>
        );
    }
}

LoginView.propTypes = {

};

const styles = StyleSheet.create({
    container:{
        flex: 1,
        justifyContent:"center",
        alignContent:"center",
        backgroundColor: "#FFF"
    }
    });


export default LoginView;