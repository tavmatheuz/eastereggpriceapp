import React from "react";
import { Root } from "native-base";
import { StackNavigator, DrawerNavigator } from "react-navigation";

// import Setup from "./src/boot/setup";
import LoginView from './src/components/login';
import EggListView from './src/components/eggList';
import SideBar from './src/components/sidebar';
import EggDescriptionView from './src/components/eggDescription';

// export default class App extends React.Component {
//   render() {
//     return <Setup />;
//   }
// }

const Drawer = DrawerNavigator(
  {
    EggListView: { screen: EggListView },
    LoginView: { screen: LoginView }
  },
  {
    initialRouteName: "LoginView",
    contentOptions: {
      activeTintColor: "#e91e63"
    },
    contentComponent: props => <SideBar {...props} />
  }
);

const AppNavigator = StackNavigator(
  {
    Drawer: { screen: Drawer },

    LoginView: { screen: LoginView },
    EggListView: { screen: EggListView },
    EggDescriptionView: { screen: EggDescriptionView }
  },
  {
    initialRouteName: "Drawer",
    headerMode: "none"
  }
);

export default () =>
  <Root>
    <AppNavigator />
  </Root>;
